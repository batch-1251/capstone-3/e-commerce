import React, {Fragment, useContext} from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';



import {Navbar, Nav, Button} from 'react-bootstrap';

import UserContext from './../UserContext';


export default function AppNavbar(){
	// destructure context object
	const {user, unsetUser} = useContext(UserContext);

	//useHistory is a react-router-dom hook
	let history = useHistory();


	const logout = () => {
	  unsetUser();
	  history.push('/login');
	}

	let leftNav = (user.id !== null) ? 
	      (user.isAdmin === true) ?
	        <Fragment>
	          <Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
	          <Nav.Link onClick={logout}>Logout</Nav.Link>
	        </Fragment>
	      :
	        <Fragment>
	        	<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
	        	<Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
	         	<Nav.Link onClick={logout}>Logout</Nav.Link>
	        </Fragment>
	  :
	    (
	      <Fragment>
	          <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
	          <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
	        </Fragment>

	    )


	return(

		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
		  <Navbar.Brand as={NavLink} to="/">Gaming GPU Shop</Navbar.Brand>
		  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		  <Navbar.Collapse id="responsive-navbar-nav">
		    <Nav className="mr-auto">
		      
		     
		    </Nav>
		    <Nav>
			    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
			    
			</Nav>
			<Nav>
			  {leftNav}
			</Nav>
		  </Navbar.Collapse>
		</Navbar>

		)
}