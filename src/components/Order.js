import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

/*react-bootstrap components*/
import {Card, Button, Table} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Order({orderProp}){
	console.log(orderProp)
	

	const [{name, description, price, _id}] = orderProp


	return(
		<Table striped bordered hover>
		  <thead>
		    <tr>
		      
		      <th>Product Name</th>
		      <th>Description</th>
		      <th>Price</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      
		      <td>{name}</td>
		      <td>{description}</td>
		      <td>{price}</td>
		    </tr>
		    <br/>
		    	<tr>
		    	  
		    	  <td></td>
		    	  <td></td>
		    	  <td>
		    	  	  <div className="text-center">
		    	  	    	<Link className="btn btn-dark" to='/'>
		    	  			    		Check Out
		    	  			    	</Link>
		    	  	    </div>
		    	  </td>
		    	</tr>
		    
		  </tbody>
		  
		</Table>
	)
}

