import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

/*react-bootstrap components*/
import {Card, Button,Container,Row,Col,Table,Image} from 'react-bootstrap';

import {Link} from 'react-router-dom';

import Style from './Style.module.css';



export default function Product({productProp}){

	

	

	const {name, description, price, _id,} = productProp



	return(
		<div>
	
		<Container className="mr-5">
		    <Card style={{ width: '30rem', height:'15rem'}} className={Style.productInfo}>
		      	<Card.Title>{name}</Card.Title>
		      	<h5>Description</h5>  	  		
		      	<p>{description}</p>
		      	<h5>Price:</h5> 	  		
		      	<p>{price}</p>
		      	  	  		  	  		

		      	<Link className="btn btn-dark" to={`/products/${_id}`} style={{ width: '15rem' }}>
		      	  	Add to cart
		      	</Link>

		    </Card>
		    
		</Container>
		</div>
		 
				
		
		  	
		  	);
	
		 
	
	}
