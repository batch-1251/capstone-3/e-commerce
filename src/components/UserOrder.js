import React, {useState, useEffect, Fragment} from 'react'

import {Container, Table, Button, Modal, Form} from 'react-bootstrap'

import Order from './Order';




export default function UserOrder({orderData}){

	console.log(orderData)

	const [orders, setOrders] = useState([])

	useEffect( () => {
		const ordersArr = orderData.map( order => {
		
			
				return <Order key={order._id} orderProp={order}/>
			
		})
		setOrders(ordersArr)
	}, [orderData])

	return(
		<Container>
			
			{orders}
		</Container>
	)
}