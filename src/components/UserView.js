import React, {useState, useEffect,} from 'react';

import {Container, Row,Col,Card} from 'react-bootstrap';

import Product from './Product';


import Style from './Style.module.css';




export default function UserView({productData}){

	

	const [products, setProducts] = useState([])

	useEffect( () => {
		const productsArr = productData.map( product => {
			// console.log(course)
			if(product.isActive === true){
				return <Product key={product._id} productProp={product}/>
			} else {
				return null
			}
		})
		setProducts(productsArr)
	}, [productData])

	const imageInfo=[

		{image:"./../images/gforce-gtx1070.png"},
		{image:"./../images/gforce-gtx1650.jpg"},
		{image:"./../images/gforce-rtx2060.jpg"},
		{image:"./../images/gforce-rtx2080.jpg"},
		{image:"./../images/radeon-5500xt.png"},
		{image:"./../images/gforce-gtx970.jpg"},

	]

		const renderImages=(image,index)=>{

		return(

			<Card style={{ width: '15rem' }} key={index} className={Style.productImg}>
			  <Card.Img variant="top" src="holder.js/100px180" src={image.image}/>
			  		
			</Card>
			


			)
	}

	return(
		<Container>
			<Row>
				<Col xs={6}>{imageInfo.map(renderImages)}</Col>
				<Col xs={6}>{products}</Col>
			</Row>
			
		
			
			
		</Container>
	)
}