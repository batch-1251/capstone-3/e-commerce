import React, {useState, useEffect,useContext} from 'react';

import {Container, Row,Col,Card} from 'react-bootstrap';

import UserOrder from './../components/UserOrder';

import UserContext from './../UserContext';



export default function Cart(){

	const [orders, setOrders] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://calm-temple-20940.herokuapp.com/api/users/myOrders',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			setOrders(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

	return(
		<div>

		<UserOrder orderData={orders} />
			
		</div>
					
		
	)
}