import React from 'react';

import {Jumbotron, Button, Carousel, Container, Row, Col, Image} from 'react-bootstrap';

import Style from './../components/Style.module.css';

import {Link} from 'react-router-dom';


export default function Home(){


	return(
		<div>
			<Jumbotron fluid className={Style.bg}>
			<br/><br/>
			  <h1>Boost your Gaming Eperience</h1>
			  <p>
			    Check out our shop, upgrade your grahpic card and game on to the best level.
			  </p>
			  <p><br/><br/>
			    <Link to="/register" to="/products"><Button className={Style.button} variant="dark">SHOP NOW!</Button></Link>
			  </p>
			  <br/><br/><br/><br/><br/><br/>
			</Jumbotron>

		{/*---------------------------------------------------------*/}
			<Carousel>
			  <Carousel.Item interval={2000}>
			    <Container>
			      <Row>
			        <Col xs={6} md={3}>
			          <Image src="./../images/gforce-gtx1070.png" thumbnail fluid />
			        </Col>
			        <Col xs={6} md={3}>
			          <Image src="./../images/gforce-gtx1650.jpg" thumbnail fluid />
			        </Col>
			        <Col xs={6} md={3}>
			          <Image src="./../images/gforce-rtx2060.jpg" thumbnail fluid />
			        </Col>
			        <Col xs={6} md={3}>
			          <Image src="./../images/gforce-rtx2080.jpg" thumbnail fluid />
			        </Col>
			      </Row>
			    </Container>
			    
			  </Carousel.Item>
			  <Carousel.Item interval={2000}>
			    <Container>
			      <Row>
			        <Col xs={6} md={3}>
			          <Image src="./../images/radeon-5500xt.png" thumbnail fluid />
			        </Col>
			        <Col xs={6} md={3}>
			          <Image src="./../images/gforce-gtx970.jpg" thumbnail fluid />
			        </Col>
			        <Col xs={6} md={3}>
			          <Image src="./../images/gforce-gtx1080ti.jpg" thumbnail fluid />
			        </Col>
			        <Col xs={6} md={3}>
			          <Image src="./../images/gforce-rtx3080.jpg" thumbnail fluid />
			        </Col>
			      </Row>
			    </Container>
			   
			  </Carousel.Item>
			  <Carousel.Item interval={2000}>
			    <Container>
			      <Row>
			        <Col xs={6} md={3}>
			          <Image src="./../images/gforce-gtx980ti.jpg" thumbnail fluid />
			        </Col>
			        <Col xs={6} md={3}>
			          <Image src="./../images/radeon-rx580.jpg" thumbnail fluid />
			        </Col>
			        <Col xs={6} md={3}>
			          <Image src="./../images/radeon-rx460.jpg" thumbnail fluid />
			        </Col>
			        <Col xs={6} md={3}>
			          <Image src="./../images/gforce-gtx1050ti.jpg" thumbnail fluid />
			        </Col>
			      </Row>
			    </Container>
			    
			  </Carousel.Item>
			</Carousel>
		</div>
		)
}